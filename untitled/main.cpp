#include "widget.h"
#include <QApplication>
#include <QPushButton>
#include <QPainter>
#include <QPaintEvent>
#include <QMediaPlayer>
#include <QMediaContent>
#include <iostream>

class ImageButton : public QPushButton
{
 Q_OBJECT
 public:
    ImageButton() = default;
    ImageButton(QWidget *parent);
    void paintEvent(QPaintEvent *e) override;
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

 public slots:
    void SetUp();
    void SetDown();
    QPixmap getCurrentButtonStatus ();
    QPixmap getmButtonDownPixmap ();
    QPixmap getmButtonUpPixmap ();
 private:
    QPixmap mCurrentButtonPixmap;
    QPixmap mButtonDownPixmap;
    QPixmap mButtonUpPixmap;
    QMediaPlayer *plyr = new QMediaPlayer(nullptr);
};

ImageButton::ImageButton(QWidget *parent)
{
    setParent(parent);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    mButtonUpPixmap = QPixmap( "./button_up.png");
    mButtonDownPixmap = QPixmap("./button_down.png");
    mCurrentButtonPixmap = mButtonUpPixmap;
    setGeometry(mCurrentButtonPixmap.rect());

    plyr->setMedia(QUrl::fromLocalFile("./BttnCkck.mp3"));
    plyr->setVolume(100);
}

QPixmap ImageButton::getCurrentButtonStatus()
{
    return mCurrentButtonPixmap;
}

QPixmap ImageButton::getmButtonUpPixmap()
{
    return mButtonUpPixmap;
}

QPixmap ImageButton::getmButtonDownPixmap()
{
    return mButtonDownPixmap;
}

void ImageButton::paintEvent(QPaintEvent *e)
{
    QPainter p(this);
    p.drawPixmap(e->rect(), mCurrentButtonPixmap);
}

QSize ImageButton::sizeHint() const
{
    return QSize(100, 100);
}

QSize ImageButton::minimumSizeHint() const
{
    return sizeHint();
}

void ImageButton::SetDown()
{
    mCurrentButtonPixmap = mButtonDownPixmap;
    plyr->play();
    update();
}

void ImageButton::SetUp()
{
    mCurrentButtonPixmap = mButtonUpPixmap;
    plyr->play();
    update();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    auto *plyr = new QMediaPlayer;

    ImageButton redButton(nullptr);
    redButton.setFixedSize(100,100);
    redButton.move(1000,400);
    QObject::connect(&redButton, &QPushButton::clicked, [&redButton]()
    {
        if(redButton.getCurrentButtonStatus() == redButton.getmButtonUpPixmap())
        {
            redButton.SetDown();
        }
        else if(redButton.getCurrentButtonStatus() == redButton.getmButtonDownPixmap())
        {
            redButton.SetUp();
        }
    });
    redButton.show();
    return app.exec();
}

#include "main.moc"
